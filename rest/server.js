const express = require("express");
const bodyParser = require("body-parser");
const dotEnv = require("dotenv");
const path = require('path');
const morgan = require('morgan');
const knex = require('../knex');

const app = express();


dotEnv.config({ path: "./config.env" });

app.set('views', path.join(__dirname, "views"));
app.set('view engine', 'ejs');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended:false }));
app.use(bodyParser.json());


require('./controllers/ejsRoute')(app);

app.get('/select', (req, res) => {
   knex.select().from('tarea').then( tabla => {
       res.send(tabla);
   });
});

app.listen(process.env.PORT || 3000, _ =>{
    console.clear();
    console.log(`Server is running on port ${process.env.PORT}`);
});