
module.exports = (app) => {

    let entradas = [];
    app.locals.entries = entradas;

    app.get("/", (req, res) =>{
        res.render('index', {
            title: "HOMEPAGE"
        });
    });
    
    app.get("/newEntry", (req, res) =>{
        res.render('new_entry', {
            miTitulo: "Este es mi título",
            miBody: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, consequuntur quidem voluptatum velit et debitis praesentium iure possimus in minima veritatis? Dolores neque debitis aliquid sit dolorum perspiciatis tempore doloribus?'
        });
    });
    
    app.post("/newEntry", (req, res) =>{
        const { title, body } = req.body;
        console.log(req.body);
        const date = new Date();

        console.log(date.getHours)
        
        let tarea = {
            titulo: title,
            contenido: body,
            fechaPub: `Año: ${date.getFullYear()} día: ${date.getDay()} mes: ${date.getMonth()} hora: ${date.getHours()}`
        };

        entradas.push(tarea);

        console.log(entradas);

        res.redirect("/");
    });

};